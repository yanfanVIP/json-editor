import SelectTable from '../SelectTable'

const buildColumn = (schema, config={}, index) => {
  const column = {
    title: config.title || schema.title,
    key: config.key || schema.key,
    dataIndex: config.dataIndex || schema.key,
    valueType: config.formValueType || buildValueType(schema, config),
    formItemProps: config.formItemProps || buildFormItemProps(schema, config),
    fieldProps: config.fieldProps || buildFieldProps(schema, config),
    convertValue: config.convertValue || buildConvertValue(schema, config),
    transform: config.transform || buildTransform(schema, config),
    width: config.width || buildWidth(schema, config),
    sort: config.sort || index,
    colProps: config.colProps || buildColProps(schema, config),
    ...config
  }
  const formItem = buildFormItem(schema, config)
  if(formItem){ column.renderFormItem = formItem }
  if(schema.primary){ column.hideInForm = true }
  return column
}

const buildValueType = (schema, config) => {
  if(schema.key == 'password'){ return 'password' }
  if(config.relation){ return 'select' }
  switch (schema.type) {
    case 'bit': return 'switch'
    case 'int': return 'digit'
    case 'tinyint': return 'digit'
    case 'mediumint': return 'digit'
    case 'bigint': return 'digit'
    case 'decimal': return 'digit'
    case 'double': return 'digit'
    case 'float': return 'digit'
    case 'date': return 'date'
    case 'time': return 'time'
    case 'datetime': return 'dateTime'
    case 'timestamp': return 'dateTime'
    case 'year': return 'dateYear'
    case 'varchar': return 'text'
    case 'char': return 'text'
    case 'tinytext': return 'textarea'
    case 'mediumtext': return 'textarea'
    case 'text': return 'textarea'
    case 'longtext': return 'textarea'
    case 'json': return 'code'
    case 'binary': return 'image'
    case 'varbinary': return 'image'
    case 'tinyblob': return 'image'
    case 'blob': return 'image'
    case 'mediumblob': return 'image'
    case 'longblob': return 'image'
    default: return 'text';
  }
}

//Form.Item控件的配置
const buildFormItemProps = (schema, config) => {
  if(schema.must){ return { rules: [{ required: true, message: '此项为必填项' }] } }
}

//输入控件的配置
const buildFieldProps = (schema, config) => {
  switch (schema.type) {
    case 'int': return { precision: 0 }
    case 'tinyint': return { precision: 0 }
    case 'mediumint': return { precision: 0 }
    case 'bigint': return { precision: 0 }
    default: return {};
  }
}

const buildFormItem = (schema, config) => {
  if(config.relation){ return (form,config)=>(<SelectTable fieldProps={config} table={schema.relation.table} multi={false}/>) }
  if(["json"].includes(schema.type)){ return (form, config) => <JSONCode fieldProps={config} />}
}

const buildTransform = (schema, config) => {
  return (value) => { return value }
}

const buildConvertValue = (schema, config) => {
  return (obj)=> obj
}

const buildWidth = (schema, config) => {
  switch (schema.type) {
    case 'tinytext': return "xxl"
    case 'mediumtext': return "xxl"
    case 'text': return "xxl"
    case 'longtext': return "xxl"
    case 'json': return "xxl"
  }
  return "md"
}

const buildColProps = (schema, config) => {
  switch (schema.type) {
    case 'tinytext': return { xs: 24, md: 24 }
    case 'mediumtext': return { xs: 24, md: 24 }
    case 'text': return { xs: 24, md: 24 }
    case 'longtext': return { xs: 24, md: 24 }
    case 'json': return { xs: 24, md: 24 }
  }
  return { xs: 24, md: 12 }
}

export default (table_config, editable=false) => {
  const schemas = table_config.schema || []
  const columns = table_config.config?.columns || []
  const formExtended = table_config.config?.formExtended || []
  const forms = schemas.map((schema, index)=>buildColumn(schema, {
    ...columns[schema.key], 
    table:table_config.table,
    relation: table_config.config.relation ? table_config.config.relation[schema.key] : null
  }, index)).sort((a,b)=>a.sort - b.sort)
  formExtended && formExtended.forEach(ext=>{ 
    const exist = forms.findIndex(f=>ext.key ==f.key)
    if(exist >= 0){
      forms[exist] = {...forms[exist], ...ext}
    }else{
      forms.push(ext) 
    }
  })
  if(!editable){
    forms.forEach(form => {
      form.fieldProps == form.fieldProps || {}
      if(["date", "time", "dateTime", "dateYear"].includes(form.valueType)){
        form.fieldProps.disabled = true
      }
      form.fieldProps.readOnly = true
    })
  }
  return forms.sort((a,b)=>a.sort - b.sort)
}