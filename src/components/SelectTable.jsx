import React from "react"
import { ProForm } from '@ant-design/pro-components';
import { Input } from 'antd'
import { SettingOutlined } from '@ant-design/icons'


const SelectTable = function Component(props){

  return (
    <ProForm.Item {...props}>
      <Input placeholder="Select ..." addonAfter={<SettingOutlined/>}/>
    </ProForm.Item>
  )
}

export default SelectTable