import { defineConfig } from 'vite'
import { resolve } from 'path'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default (CONFIG) => {
  console.log(`=======  BUILD ${CONFIG.mode} ========`)
  return defineConfig({
    plugins: [react()],
    define:{
      global: 'window'
    },
    build: {
      emptyOutDir: false,
      assetsDir: 'resources',
      rollupOptions: {
        input: resolve(__dirname, `${CONFIG.mode}.html`),
        output:{
          entryFileNames: `resources/${CONFIG.mode}.js`
        },
      }
    },
    server:{
      open:true,
      port: 3334,
      proxy: {
        '/api': {
          target: 'http://localhost:3000',
          changeOrigin: true,
        }
      }
    }
  })
  
}